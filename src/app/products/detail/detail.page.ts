import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ProductService } from '../product.service';
import { lugares } from '../products.model';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  lugar: lugares;
  constructor(
    private activeRouter: ActivatedRoute,
    private productService: ProductService,
    private router: Router,
    private alertController: AlertController
  ) { }
  
  ngOnInit() {
    
    this.activeRouter.paramMap.subscribe(
      paramMap => {
        if(!paramMap.has('lugarId')){
          return;
        }
        const lugarId =  parseInt( paramMap.get('lugarId'));
        this.lugar = this.productService.getLugar(lugarId);
        console.log(lugarId);
      }
    );
  }

  deleteLugar(){
    this.alertController.create({
      header: "Borrar Lugar",
      message: "¿Esta seguro que desea borrar este lugar?",
      buttons:[
        {
          text:"No",
          role: 'no'
        },
        {
          text: 'Borrar',
          handler: () => {
            this.productService.deleteLugar(this.lugar.codigo);
            this.router.navigate(['./products']);
          }
        } 
      ]
    })
    .then(
      alertEl => {
        alertEl.present();
      }
    );
    
  }

  presentAlert() {
    const alert = document.createElement('ion-alert');
    alert.cssClass = 'my-custom-class';
    alert.header = 'Reserva';
    alert.subHeader = 'Usted ha reservado este sitio.';
    alert.message = 'Gracias por confiar en nosotros';
    alert.buttons = ['OK'];
  
    document.body.appendChild(alert);
    return alert.present();
  }
  
}
